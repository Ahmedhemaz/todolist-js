$(document).ready(function() {

 // We will refer to $calendar in future code
 var $calendar = $("#calendar").fullCalendar(
 { // Start of options
     themeSystem: 'bootstrap4',
     header: 
     {
     left: 'prevYear,nextYear',
     center: 'title',
     right: 'today,month, prev,next'
     },
     allDayDefault: false,
     selectable: true,
     selectHelper: true,
     editable: true,
     eventLimit: true, // allow "more" link when too many events
 }
 );
 });
    
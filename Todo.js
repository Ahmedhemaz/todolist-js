// TODO CONTROLLER
var todoController = (function () {
    
    // todo item constractor to add alot of items
    var TodoItem = function (id, description) {
        this.id = id;
        this.description = description;
    };
    
    // array to save all items to it
    var allTodoItems = [];
    
    var findIdIndex = function (id) {
        var ids, index;
            ids = allTodoItems.map(function (current) {
                return current.id;
            }); // method creates a new array with the results of calling a provided function (objects of TodoItem in allTodoItems array and return the ids in new array)
            index = ids.indexOf(id);
        return index;
        
    };
    return {
        addItem: function (des) {
            var newItem, ID;
            if (allTodoItems.length > 0) {
                ID = allTodoItems[allTodoItems.length - 1].id + 1;
            } else {
                ID = 0;  
            }
            // create new item
            newItem = new TodoItem(ID, des); // creating new instance from TodoItem  constractor
            // push it to the allTodoItems array
            allTodoItems.forEach(element => {
                if(element.description!=des){
                 console.log("asd");
                }
            });
            allTodoItems.push(newItem);   
            //return the item to use it in other controllers
            localStorage.setItem("all",JSON.stringify(allTodoItems));
            return newItem;
        },
        
        deleteItem: function (id) {
            
            var index = findIdIndex(id);
            if (index!== -1) {
                allTodoItems.splice(index, 1);
                localStorage.setItem("all",JSON.stringify(allTodoItems));
                }
        },
        
        editItem: function (id,newdes) {
            var index = findIdIndex(id);

            
            if (index !== -1) {
                allTodoItems[index].description = newdes;
                localStorage.setItem("all",JSON.stringify(allTodoItems));
            }
            
            
            
        },
        
        testing: function() {
            console.log(allTodoItems);
        }
    };
    
})();




// UI CONTROLLER
var UIController = (function () {
    
    var DOMstring = {
        
        todoAddItem: 'additem',
        inputButton:'addbtn',
        todoListContainer:'.todo-items-list',
        itemDescription:'.item-description',
        editLabel:'.item-description label',
        editInputText:'.item-description input[type="text"]'
        
    };
    
    return {
        //1 get input from input field from html 
        getInput: function () {
            
            return {
             todoItem: document.getElementById(DOMstring.todoAddItem).value
                }; 
        },
        //2 add items created to list at the UI
        addListItem: function (obj) {
            var html,newHtml;
            // Create HTML string with placeholder text
            
           html = '<div class="item-clearfix" id="todoitem-%id%"><div class="item-description"><label>%description%</label><input type="text"></div><div class="edit-delete-item"> <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-trash-o" aria-hidden="true"></i></div></div>'
            
           
            // Replace the placeholder with some data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);

            
            // insert the html to the dom
            document.querySelector(DOMstring.todoListContainer).insertAdjacentHTML('beforeend', newHtml);
            
            
        },
        //3 edit items
        editListItem: function(elementID) {
            
            var el;
            el = document.getElementById(elementID);
            editLabel = el.childNodes[0].firstElementChild;
            editInputText = el.childNodes[0].lastElementChild;

                editLabel.style.display='none';
                editInputText.style.display='block';
                editInputText.focus();
                editInputText.value = editLabel.innerText ;
            
            
            document.addEventListener('keypress', function(event) { 
                if(event.keyCode === 13 || event.which === 13) {
                    editLabel.innerText  = editInputText.value ;
                    editLabel.style.display='block';
                    editInputText.style.display='none';
            } 

        

            });  

            return editInputText.value;
            
            

        },



        //5 delete items     
        deleteListItem: function (elementID) {
            
            // element id then select parent node then delete child 
            
            var el = document.getElementById(elementID);
            el.parentNode.removeChild(el); 
        },
        //6 clear fields 
        
        clearFields: function() {
            
            document.getElementById(DOMstring.todoAddItem).value=""; 
        },

        
        getDOMstrings: function() {
            return DOMstring;
        }
        
    };
      
})();




// GLOBAL CONTROLLER
var controller = (function(todoCtrl,UICtrl) {
    
    
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();
    
        document.getElementById(DOM.inputButton).addEventListener('click', function () {
                ctrAddItem(); 
            
            });
            document.addEventListener('keypress', function(event) { 
        if(event.keyCode === 13 || event.which === 13) {
            ctrAddItem(); 
            
            }
        });   
        
        document.querySelector(DOM.todoListContainer).addEventListener('click',ctrlDeleteItem);
        
        document.querySelector(DOM.todoListContainer).addEventListener('click',ctrlEditItem);
                
    };

    
    
    var ctrAddItem = function () {
        var input,newItem
        
        // Get the input data
         input = UICtrl.getInput();
        
        if(input.todoItem !== "") {
            
            // add items to todo controller
            newItem = todoCtrl.addItem(input.todoItem);
        
            //add item to the UI
            UICtrl.addListItem(newItem);
        
            //clear fields
            UICtrl.clearFields();  
        }
           
    }; 

    var ctrAddOldItem = function (oldDes) {
        var input,newItem
        
        // Get the input data
         input = oldDes;
        
        if(localStorage.getItem("all")!=null) {
            
            // add localStorage items to todo controller
            newItem = todoCtrl.addItem(oldDes);
        
            //add item to the UI
            UICtrl.addListItem(newItem);
        }
           
    };
    var ctrlDeleteItem = function (event) {
        
        if (event.target.className === 'fa fa-trash-o') {
            var itemID, splitID,ID;
            // take the id from the html parent node
            itemID = event.target.parentNode.parentNode.id;
            if (itemID) {

                // split id to take id number 
                splitID = itemID.split('-');

                // convert the string into int
                ID = parseInt(splitID[1]) ;
                
                // delete item from the data structure
                todoCtrl.deleteItem(ID);

                // delete item from UI
                UICtrl.deleteListItem(itemID);

            }   
        } 
    };
    
    var ctrlEditItem = function (event) {
        
        if (event.target.className === 'fa fa-pencil') {
            var itemID, splitID,ID,edit;
            // take the id from the html parent node
            itemID = event.target.parentNode.parentNode.id;
            if (itemID) {
                // split id to take id number 
                splitID = itemID.split('-');

                // convert the string into int
                ID = parseInt(splitID[1]) ;
                
                
                //edit item from UI
                 edit = UICtrl.editListItem(itemID);                
            }
            
        }
        return ID;
            
    };
        
    
    return {
        init: function () {
            if(localStorage.getItem("all")){
                var items = localStorage.getItem("all");
                items = JSON.parse(items);
                items.forEach(element => {
                    ctrAddOldItem(element.description);
                });
                console.log(items);
            }
            setupEventListeners();
        }
    };

})(todoController, UIController);

controller.init();